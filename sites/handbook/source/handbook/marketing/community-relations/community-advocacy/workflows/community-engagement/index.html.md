---
layout: handbook-page-toc
title: "Community Engagement"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview
Community advocates' core mission is to respond to questions about GitLab asked online. To support this mission, we monitor different social networks and respond ourselves or [involve one of our brilliant experts](/handbook/marketing/community-relations/community-advocacy/workflows/involving-experts/). While we are able to connect our team members with the wider community using this approach, we realize that it might be a bit intrusive. That's why we're saving this workflow only for the comments that require expert's response. 

While this is working for the most important social interactions, it also leaves a lot of useful feedback disregarded. This program is trying to increase engagement of our knowledgeable team members and give them the opportunity to 
- receive direct feedback about GitLab and learn how our community perceives us
- grow their social presence and build new relationships with our wider community
- learn more about our competition and latest tech trends
- help spread the word about our company and values

What we're hoping to achieve:
- Humanize and diversify the GitLab voice by increasing the number of team members that are engaging with our wider community
- Provide a better experience for our wider community by letting our team members personally react to their feedback
- Lower the barrier for someone who is starting their journey in an online community
- Invite all team members (not just community relations team) to participate in our community - "Everyone can contribute"

## Workflow
### GitLab team members
GitLab team members who want to grow their engagement with our community will volunteer to join #community-involvement Slack channel. On their own time, team members can pick from tweets in the channel and respond from their personal accounts.

Tweets will be manually filtered by community advocacy team and will typically consist of: positive feedback or experience that would be nice to recognize by our team members, discussions or questions about product, support, sales, DevOps, competition, etc.

### Advocates workflow
1. If a ticket doesn't require an expert's response, decide if there is a potential for building a relationship with our community
1. Apply `Involving Experts::Community Involvement` macro
1. Manually paste the URL to the related tweet in [community-involvement Slack channel](https://gitlab.slack.com/messages/community-involvement)
1. If one of our team members respond, it will reopen the related ticket in our Zendesk. Use `Involving Experts::Expert Responded` macro

## How to start engaging
### Setting up your Twitter account
1. Use your individual account. The goal is to increase the number of individual accounts that are directly engaging with our community members besides the official GitLab handle.
1. We encourage updating your profile to disclose that you work at GitLab.

### Tone of voice and delivery
1. Stay positive and assume good faith.
1. Remember that you are speaking to a human. A simple acknowledgment, "Thank you" or "How can we make it better?" is sometimes enough.
1. It's okay to use emojis!
1. Try to add value. Either express gratitude or provide additional information (preferably supported with links)
1. Be interesting and on point - avoid using typical marketing phrases

### What to do
1. Start on a positive note - thank them for their comment.
1. Always seek feedback. Link to the related issue and encourage them to leave a comment or send a link to the relevant issue tracker and invite them to open a new one.
1. Address any and all points the user has made. Don’t be afraid to tell that you don’t know the answer and that you'll look into it.
1. You’re allowed to disagree. Try to inform the person (respectfully) why they might be misguided and provide any links they may need to make a more informed decision.
1. Be open minded, curious, kind, welcoming, transparent, direct, and honest. Also, be a role model of our values.
1. Instead of just upvoting or saying a one word “Thanks!”, try personalizing your response.

### What not to do
1. Do not engage in competitor bashing. Instead, try focusing on positive differences that GitLab brings to the table when suitable
1. Don't feel the need to defend GitLab. Try acknowledging their opinion instead. Negative feedback can also be a great learning opportunity
1. Don't respond if things are becoming personal - we recommend stepping away and maybe involving community advocates
1. Always bear in mind: "Don't argue but represent"

### Responding to criticism
1. Acknowledge their comment/concern/our mistake
1. Apologize for the inconveniences
1. Ask how we can improve and invite them to participate in solving the problem by linking to the related issues. Turn it into a collaborative discussion.
1. End on a positive note. You may need to add some extra words to soften your tone.

### Additional resources
1. If you don't feel confident to start engaging, we can schedule a training/see this training recording/see these examples
1. We are happy to schedule some 1-1 time with you if you need some help to get started. Also, we're planning a [Learning & Development](/handbook/people-group/learning-and-development/) session that will be recorded.
1. Don't hesitate to reach out at [#community-advocacy](https://gitlab.slack.com/messages/community-advocates) Slack channel. If something is urgent, ping us using @advocates
1. Pro tip: If something seem like a question that is frequently asked, try searching for an answer in our [Support Zendesk](https://gitlab.zendesk.com/) instance, or on our [community forum](https://forum.gitlab.com/)
