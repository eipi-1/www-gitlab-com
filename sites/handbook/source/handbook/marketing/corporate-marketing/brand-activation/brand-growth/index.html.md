---
layout: handbook-page-toc
title: "Brand Growth"
description: "Brand Growth Handbook page."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


# <i class="fab fa-gitlab fa-fw" style="color:rgb(252,109,38); font-size:.85em" aria-hidden="true"></i> Brand Growth
{: #tanuki-orange} 
{:.no_toc}

![Brand Growth Team Image](/images/handbook/marketing/corporate-marketing/brand-activation/home_office_character_1.png){: .shadow.medium.center}

## What our team does


#### Responsibilities
- **Overview** - Our team works in collaboration with all of GitLab to grow the brand. We... 
   - Identify how our brand is currently positioned in the market and develop a reporting process to document the progress of our brand
   - Implement brand measurement systems so the value of our brand can be easily tracked
   - Oversee the brand message creation process, as well as make sure the copy on our web properties align with the message
   - Develop and execute marketing campaigns aimed at communicating our brand message
   - Educate and communicate our brand personality internally and align company around the message
   - Develop brand guidelines and communication programs so entities outside of GitLab, can easily understand how to use our brand
   - Brand management for international expansion, including production of brand guidelines in collaboration with the Brand team.
   - Measure and report on success of brand marketing campaigns
   - Oversee merchandise management
   - Provide support with brand team process establishment and documentation
   - Lead Brand Growth Content team
   - Are stewards of the [company brand](/company/brand/)

| Person | Leads|
| ------ | ------ |
| Becky Reich | Team, strategy, guidelines, and data |
| Ash Withers | Brand growth campaigns, brand message, brand growth content |
| Emily Landers | Mechandise managment |

#### Goals

How we track [Link to Brand tracker] <br>
- Brand awareness 
- Brand attribute health 
- Brand message permeation 



#### Quarterly OKRs
- FY22
  - [Q1](https://gitlab.com/groups/gitlab-com/marketing/corporate_marketing/-/epics/76#q1-themes)
  - [Q2](https://gitlab.com/groups/gitlab-com/marketing/corporate_marketing/-/epics/76#q2-themes)
  - Q3 - Coming soon
    - [Q3 Candidates](https://gitlab.com/groups/gitlab-com/marketing/corporate_marketing/-/epics/90)
  - Q4 - Coming soon 
    - [Q4 Candidates](https://gitlab.com/groups/gitlab-com/marketing/corporate_marketing/-/epics/91)
- [Ideas](https://gitlab.com/groups/gitlab-com/marketing/corporate_marketing/-/epics/93)
   - This epic holds ideas that don't have a quarter candidate placement yet

## Who we are


- [Becky Reich](http://about.gitlab.ex1.ipv6.cosl.com.cn:443/company/team/#rreich) 
- [Ash Withers](http://about.gitlab.ex1.ipv6.cosl.com.cn:443/company/team/#awithers) 

## How to work with us


#### How we determine what we prioritize 
We prioritize our qurterly OKRs based on our team's goals, the goals of marketing and the goal of the company, ensuring that any company OKRs relevant to our work are prioritized first. When we receive an issue we will ask ourselves these questions: 

1. Is this an existing OKR? 
1. Does this help support our team goals?
1. Can we make a quick MVC or is this a larger project 

#### Requsting Support
- Proposal Issue Issue Template (LINK COMING SOON)
- General Request Issue Template (LINK COMING SOON)     


#### Setting yourself up for success
- See resources and guidelines [HERE](https://about.gitlab.com/handbook/marketing/corporate-marketing/brand-activation/brand-growth/#resources-and-guidelines)

#### The process and timing
- We have a [general process](https://about.gitlab.com/handbook/marketing/corporate-marketing/brand-activation/brand-growth/#our-planning-process) and will continue to develop [indepth processes and timing](https://about.gitlab.com/handbook/marketing/corporate-marketing/brand-activation/brand-growth/#deep-dive)

#### Cross-functional OKR candidates
To submit a cross-functional OKR Candidate go to the Epic, located [here](https://about.gitlab.com/handbook/marketing/corporate-marketing/brand-activation/brand-growth/#quarterly-okrs) for the quarter you want to propose the candidate and follow the process outlined in the Epic description. 

## Resources and Guidelines

  {::options parse_block_html="true" /}
  
<details>
  <summary markdown="span">How we use GitLab</summary>

### How we use GitLab

- How we use epics 
   - In an effort to have a SSoT epics should not contain duplicative information
   - Project stratety, goals, metrics and a working group should be in the epic
   - The production issues should be nested under each stage
   <br>
- How we use issues
   - Each step in the process should have its own issue
   - We create issue templates for as much standardized asks as possible
   - We follow the process set out in each team's handbook for how they use their requesting issues
   <br>
- How we use labels
  - All issues should have our **TEAM** label and the appropriate **STATUS** label
    - Team label: `brand growth` 
    - Status labels: these are the labels in the order they work through kanban board  
       - `mktg-status:: triage` 
          - issue is being reveiwed for consideration, consolidation, and information 
          - issue will not have an assignee or milestone
       - `mktg-status:: plan`
          - issue is ready to pulled into a milestone and given an assignee
       - `mktg-stauts:: wip`
          - issue is in a milestone iteration and actively being worked on 
       - `mktg-statuts:: review` 
          - issue is being reviewed by one or more team members
       - CLOSED: once an issue is completed it will be closed   
       <br>   

- Our [BOARD](https://gitlab.com/groups/gitlab-com/-/boards/2626234?&label_name[]=brand%20growth)
</details>

<details>
  <summary markdown="span">Templates</summary>

### Templates
Note: This section is for internal team documentation. Please visit the handbook's for specific team's to learn about their processes <br>

<details>
<summary markdown="span">Epic Templates for Production</summary>

#### Epic  Templates for production
COMING SOON
</details>

<details>
<summary markdown="span">Issue Templates for Production</summary>

#### Issue Templates for production
**PLAN** <br>
Strategy Issue - Used for creating the strategy for a project
```
---
layout: STR: Strategy Issue
title: STR: {add Project Name}
---
## Objective of Issue
Delivery strategy for {add Project Name}

## Goals of Marketing team 
- Metric 1: {add}
- Metric 2: {add}
## Final Deliverable
**Primary Objective** <br> 
{add}
**Secondary Objective(s)**
- {add}
- {add}
**Strategy** <br>
{add} 
## Roles & Responsibilities
Tag the appropriate parties below, which may be found in the [team handbook](https://about.gitlab.com/handbook/marketing/inbound-marketing/digital-experience/)
The following people are **[responsible](https://www.teamgantt.com/blog/raci-chart-definition-tips-and-example)** for completing the task or deliverable:
- [ ]  @ person 
 
The following person (there can only be one) is **[accountable](https://www.teamgantt.com/blog/raci-chart-definition-tips-and-example)** and is the last one to review the task or deliverable before it’s deemed complete:
- [ ]  @ person 
 
The following people must be **[consulted](https://www.teamgantt.com/blog/raci-chart-definition-tips-and-example)** so they can provide input:
- [ ]  @ person 
- [ ]  @ person 
- [ ]  @ person 
The following people must be **[informed](https://www.teamgantt.com/blog/raci-chart-definition-tips-and-example)** so they are kept in the loop on project progress: <br>
**CC:**
- [ ]  @ person 
- [ ]  @ person 
- [ ]  @ person 
- [ ]  @ person 
 
<!-- Please leave the label below on this issue →
~"brand growth" ~"Corporate Marketing" ~"mktg-status::plan" 
```

**CREATE** 
  - CPT: Concept

```
---
layout: (CPT) Concept Issue 
title: CPT: {add Project Name} 
---
See the epic and related issues for more detailed information. 
## Goal
Create {add amount} concept(s) that will support the strategy of this project. More specifically, {add goal}. 

#### Research Files
- [Item]()

#### Working Files​
- [Item]()

#### Scope/Deliverables
- {add item}  

#### Suggested Additional Scope/Deliverables
- {add item} 
 
#### Out of Scope
- Tactics 
- {add item} 
 
## Production
- Make sure all issues are linked and block production issues respectively 
- Add file link to the description 
- Tag anyone relevant for approvals, consults, feedback
- When complete: 
   - 1. Add the final file to the relevant production issues
   - 2. Update weight to how much time the project took
   - 3. Close issue
 
## Roles & Responsibilities
Tag the appropriate parties below, which may be found in the [team handbook](https://about.gitlab.com/handbook/marketing/inbound-marketing/digital-experience/)
 
The following people are **[responsible](https://www.teamgantt.com/blog/raci-chart-definition-tips-and-example)** for completing the task or deliverable:
- [ ]  @ person 
 
The following person (there can only be one) is **[accountable](https://www.teamgantt.com/blog/raci-chart-definition-tips-and-example)** and is the last one to review the task or deliverable before it’s deemed complete:
- [ ]  @ person 
 
The following people must be **[consulted](https://www.teamgantt.com/blog/raci-chart-definition-tips-and-example)** so they can provide input:
- [ ]  @ person 
- [ ]  @ person 
- [ ]  @ person 
The following people must be **[informed](https://www.teamgantt.com/blog/raci-chart-definition-tips-and-example)** so they are kept in the loop on project progress: <br>
**CC:**
- [ ]  @ person 
- [ ]  @ person 
- [ ]  @ person 
- [ ]  @ person 
 

~"brand growth" ~"Corporate Marketing" ~"mktg-status::plan" 
```

  - [CD: Creative Direction]()
       - For brand design team or vendor
  - CNT: Brand growth content and copy

```
---
layout: (CNT) Content Issue 
title: CNT: {add Project Name} {add type of content}
---
## Goal
{add goal}

#### Research Files
- [Item]()

#### Working Files​
- [Item]()

#### Scope/Deliverables
- {add item} 
 
#### Suggested Additional Scope/Deliverables
- {add item} 
 
#### Out of Scope
- {add item} 
 
## Production
- Make sure all issues are linked and block production issues respectively 
- Add file link to the description 
- Tag anyone relevant for approvals, consults, feedback
- When complete: 
   - 1. Add the final file to the relevant production issues
   - 2. Update weight to how much time the project took
   - 3. Close issue
 
## Roles & Responsibilities
Tag the appropriate parties below, which may be found in the [team handbook](https://about.gitlab.com/handbook/marketing/inbound-marketing/digital-experience/)
 
The following people are **[responsible](https://www.teamgantt.com/blog/raci-chart-definition-tips-and-example)** for completing the task or deliverable:
- [ ]  @ person 
 
The following person (there can only be one) is **[accountable](https://www.teamgantt.com/blog/raci-chart-definition-tips-and-example)** and is the last one to review the task or deliverable before it’s deemed complete:
- [ ]  @ person 
 
The following people must be **[consulted](https://www.teamgantt.com/blog/raci-chart-definition-tips-and-example)** so they can provide input:
- [ ]  @ person 
- [ ]  @ person 
- [ ]  @ person 
The following people must be **[informed](https://www.teamgantt.com/blog/raci-chart-definition-tips-and-example)** so they are kept in the loop on project progress: <br>
**CC:**
- [ ]  @ person 
- [ ]  @ person 
- [ ]  @ person 
- [ ]  @ person 
 

~"brand growth" ~"Corporate Marketing" ~"mktg-status::plan" 
```

  - [DSN: Web design](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/new?issuable_template=production-DE-DSN)
      - For website team
  - [Design: Brand design request](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=request-design-general)
       - For brand design team  
<br>

**PACKAGE**
   - [Brand Review](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=request-brand-review)
      - For brand design team 
<br>

**RELEASE**
  - [Launch checklist]()
  - [ENG - website](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/new?issuable_template=production-DE-ENG)

<br>  

**CONFIGURE**
  - [DATA] (COMING SOON)
  - [FEEDBACK] (COMING SOON)

<br>

**MONITOR**
    - [TRACKING - Brand tracker request] (COMING SOON)

</details>

<details>
<summary markdown="span">Campaign Templates</summary>

#### Campaign Templates - COMING SOON

</details>

<details>
<summary markdown="span">Brand Management Templates</summary>

#### Brand Management Templates

##### Z-SWOT
<figure class="video_container"><iframe src="https://docs.google.com/document/d/e/2PACX-1vS8AoryzZVlBX-IEriBjSJmE9kjzHCjeLP_2DnAZiJCmUSEkZdEdKXvcHmJOmtGJXAjRfOARL5yRgvN/pub?embedded=true"></iframe></figure>

</details>
</details>

<details>
  <summary markdown="span">Guidelines</summary>

### Guidelines

  - [Tone-of-voice](https://about.gitlab.com/handbook/marketing/corporate-marketing/#tone-of-voice-tov) 
  - [Brand Messaging](https://about.gitlab.com/handbook/marketing/corporate-marketing/#brand-message)

</details>

<details>
  <summary markdown="span">Getting in Touch</summary>

<br>

- Slack channel
  - #corp-mktg or #brand-activation
- Office hours
  - The brand growth team holds [office hours](https://calendar.google.com/event?action=TEMPLATE&tmeid=MDFibjFiaHA0bjFic3M5ZjgxYmc1YnM2NGtfMjAyMTA1MDZUMTkwMDAwWiBycmVpY2hAZ2l0bGFiLmNvbQ&tmsrc=rreich%40gitlab.com&scp=ALL) from 3:00-4:00pm (Eastern) on Thursdays. We use the time to work virtually together and welcome you to pop-in for any questions or to have a virtual work shindig 
- GitLab YouTube Unfiltered Channels 
   - [The GitLab Brand (public)](https://youtube.com/playlist?list=PL05JrBw4t0Ko9cDNHrfquYwRRTQafhGzs)
   - [Brand Growth Team (GitLab only)](https://www.youtube.com/playlist?list=PL05JrBw4t0Kq3BUqqNPhhqgXopcyzob08)
</details>

<details>
  <summary markdown="span">Deep dive guidelines</summary>

  <br>
#### Deep Dive guidelines 
Coming Soon

</details>

## Our planning process

<details>
  <summary markdown="span">Projects</summary>

#### Projects

We base our project planning based on the learning of DevOps. 

**NOTE: This is how our team orgnaizes. We are not the ICs for all these steps, but work with cross-functional teams and the larger Brand Activation team.**

1. Plan 
   - Set a proposal if needing to work cross-functionally 
   - Make a strategy issue (https://about.gitlab.com/handbook/marketing/corporate-marketing/brand-activation/brand-growth/#issue-templates-for-production) 
   - Create a timeline
   - Get dependencies and important dates 
   - Agree on tactics 
   - Create an epic with production issues 
1. Create
   - Concept 
     - Verify with working group 
   - Creative Direction
      - Design 
      - Messaging 
   -  Content 
     - Designs
     - Assets
     - Copy
1. Package - Put everything together and deliver to teams delivering on owned channels 
1. Release - 
    - Launch the project 
    - Use launch checklist (https://about.gitlab.com/handbook/marketing/corporate-marketing/brand-activation/brand-growth/#issue-templates-for-production)
1. Configure
   - Set up any monitoring dashboards 
   - Make a feedback issue that will serve to help determine scope of future iterations 
1. Monitor 
   - Review data 
      - Two weeks post-launch 
      - At end of the quarter 
<br>
CYCLE REPEATS <br>
Plan for future iterations by making MVC iteration issues out of the feedback issue and continue learning cycle 

</details>

<details>
  <summary markdown="span">Bi-weekly</summary>

#### Bi-weekly

- Start: At the beginning of an Iteration milestone we have a kick-off meeting where we: 
   - Outline what  we will aim to accomplish and add to that milestone and assign 
   - Discuss what we could pick up if we have extra time
   - Any blockers 
   - Any critical dependencies 
- End: At the end of every iteration milestone we will fill out a Geekbot survey as a retrospective. 
   - The goal is to be async for general observations. 
   - If there is anything flagged it will be discussed live. 

</details>

<details>
  <summary markdown="span">Mid-quarter</summary>

#### Mid-quarter

Halfway through the quarter we the project DRIs will:
  - Fill out the mid-way results on the OKRs
  - Give a general status of the OKR and if there is any concern it won’t be completed or any blockers the team could help move along. 

</details>

<details>
  <summary markdown="span">Quarterly</summary>

#### Quarterly

- OKR Candidate epics should be set up 2-3 quarters in advance to help with planning. 
- Three weeks before the end of the previous quarter, the next OKR planning will begin. 
- In choosing what items move forward as OKRs, at a high-level, the team considers: 
   - Projects to meet our team’s goals and that support Brand Activation and Corporate Marketing goals. 
   - Known discussions around needs for GitLab or the marketing team.
   - Existiing candidates, ideas , or proposals to move forward with and formalize. 
   - Additional projects to support cross-functional teams or projects that contribute to up-stream goals or strategy
- Epics are structured for the best possible nesting to allow anyone to see how projects ladder-up. 

</details>

<details>
  <summary markdown="span">Annually</summary>

#### Annually

At the beginning of each year we will go through these exercises
- End of year review and retrospective 
- Fiscal year planning: 
  - Strategy and playbook 
  - Budget 
  - Headcount 
- Brand management tasks that inform strategy: 
  - Trend bucket analysis and action planning 
  - Z-SWOT analysis and strategy 
  
</details>

<details>
  <summary markdown="span">Deep dive</summary>

  <br>
#### Deep Dive Processes 
Coming Soon

</details>
